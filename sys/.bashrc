if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

#if [ -f ~/.inputrc ]; then
#    . ~/.inputrc
#fi

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
export WA='/home/idanhahn/workarea/git'
export WWW='/var/www/html/'

if [ $(id -u) -eq 0 ];
then #root (make red)
  export PS1="\[\e[1;31m\][root@\W] \$\[\e[0m\] "
else
  export PS1="\[\e[1;32m\][\W] \$\[\e[0m\] "
fi
