// This node service handles comm between internet on PORT and UNIXSOCKET

var net = require('net');
/*
var server = net.createServer(function(c){

  // request server to stream to socket:
  console.log('node server connected');
  
  // call back to disconnect 
  c.on('end',function(){
    console.log('node server disconnected');
  });
  
  c.write('Node says hello\r\n');
  c.pipe(c);

});

// connection to socket
server.listen('./echo_socket', function(){
  console.log('Node server bound');
});
*/

var client = net.connect({path:'./echo_socket'},
    function() { //'connect' listener
  console.log('Node client connected');
  client.write('world!\r\n');
});
client.on('data', function(data) {
  console.log('Node recieved ' + data.toString());
  client.end();
});
client.on('end', function() {
  console.log('Node client disconnected');
});

