var http = require('http');

http.createServer(function(request,response){
	response.writeHead(200);
	response.write("ServerRunning\n");
	request.on('data',function(chunk){
		console.log(chunk.toString()+"\n");
		response.write(chunk+"\n");
	});

	request.pipe(response);
	response.write(" this is a response from pipe");

	setTimeout(function(){
		response.write("Done waiting response\n");
		console.log("done waiting\n");
		response.end();
	},8000);
	//request.on('end',function(){
	//	response.end();
	//});
}).listen(8081);

console.log('yo');