#! /bin/bash
LOGFILE=relay.log
RELAYTOOL=/home/idanhahn/workarea/relaytools
# removing com usb relay drivers:
(echo idan1980 | sudo -S rmmod 'ftdi_sio'; date) &>>  $LOGFILE
(echo idan1980 | sudo -S rmmod 'usbserial'; date) &>> $LOGFILE
(echo idan1980 | sudo -S java -Djava.library.path=".ftd2xx.so" -jar $RELAYTOOL/DenkoviRelayCommandLineTool_10.jar DAE000dY $1; date) &>>$LOGFILE
