<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function view($page = 'home')
	{
          $this->load->view('templates/header');
          $this->load->view('pages/'.$page);
          $this->load->view('templates/footer');

        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
