[fwBasic]
status = enabled
incoming = deny
outgoing = allow

[Rule0]
ufw_rule = 5900 ALLOW IN Anywhere
description = VinoServer
command = ufw allow in 5900
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5900
iface = 
logging = 

[Rule1]
ufw_rule = 5900 ALLOW OUT Anywhere (out)
description = VinoServer
command = ufw allow out 5900
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5900
iface = 
logging = 

[Rule2]
ufw_rule = 80 ALLOW IN Anywhere
description = http
command = ufw allow in 80
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 80
iface = 
logging = 

[Rule3]
ufw_rule = 80 ALLOW OUT Anywhere (out)
description = http
command = ufw allow out 80
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 80
iface = 
logging = 

[Rule4]
ufw_rule = 7500 ALLOW IN Anywhere
description = smartHomeServerTmp
command = ufw allow in 7500
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7500
iface = 
logging = 

[Rule5]
ufw_rule = 7500 ALLOW OUT Anywhere (out)
description = smartHomeServerTmp
command = ufw allow out 7500
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7500
iface = 
logging = 

[Rule6]
ufw_rule = 7002 ALLOW IN Anywhere
description = videoService
command = ufw allow in 7002
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7002
iface = 
logging = 

[Rule7]
ufw_rule = 7002 ALLOW OUT Anywhere (out)
description = videoService
command = ufw allow out 7002
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7002
iface = 
logging = 

[Rule8]
ufw_rule = 7003 ALLOW IN Anywhere
description = videoService1
command = ufw allow in 7003
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7003
iface = 
logging = 

[Rule9]
ufw_rule = 7003 ALLOW OUT Anywhere (out)
description = videoService1
command = ufw allow out 7003
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7003
iface = 
logging = 

[Rule10]
ufw_rule = 7777 ALLOW IN Anywhere
description = PS3
command = ufw allow in 7777
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7777
iface = 
logging = 

[Rule11]
ufw_rule = 7777 ALLOW OUT Anywhere (out)
description = PS3
command = ufw allow out 7777
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7777
iface = 
logging = 

[Rule12]
ufw_rule = 5900 (v6) ALLOW IN Anywhere (v6)
description = VinoServer
command = ufw allow in 5900
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5900
iface = 
logging = 

[Rule13]
ufw_rule = 5900 (v6) ALLOW OUT Anywhere (v6) (out)
description = VinoServer
command = ufw allow out 5900
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5900
iface = 
logging = 

[Rule14]
ufw_rule = 80 (v6) ALLOW IN Anywhere (v6)
description = http
command = ufw allow in 80
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 80
iface = 
logging = 

[Rule15]
ufw_rule = 80 (v6) ALLOW OUT Anywhere (v6) (out)
description = http
command = ufw allow out 80
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 80
iface = 
logging = 

[Rule16]
ufw_rule = 7500 (v6) ALLOW IN Anywhere (v6)
description = smartHomeServerTmp
command = ufw allow in 7500
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7500
iface = 
logging = 

[Rule17]
ufw_rule = 7500 (v6) ALLOW OUT Anywhere (v6) (out)
description = smartHomeServerTmp
command = ufw allow out 7500
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7500
iface = 
logging = 

[Rule18]
ufw_rule = 7002 (v6) ALLOW IN Anywhere (v6)
description = videoService
command = ufw allow in 7002
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7002
iface = 
logging = 

[Rule19]
ufw_rule = 7002 (v6) ALLOW OUT Anywhere (v6) (out)
description = videoService
command = ufw allow out 7002
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7002
iface = 
logging = 

[Rule20]
ufw_rule = 7003 (v6) ALLOW IN Anywhere (v6)
description = videoService1
command = ufw allow in 7003
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7003
iface = 
logging = 

[Rule21]
ufw_rule = 7003 (v6) ALLOW OUT Anywhere (v6) (out)
description = videoService1
command = ufw allow out 7003
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7003
iface = 
logging = 

[Rule22]
ufw_rule = 7777 (v6) ALLOW IN Anywhere (v6)
description = PS3
command = ufw allow in 7777
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7777
iface = 
logging = 

[Rule23]
ufw_rule = 7777 (v6) ALLOW OUT Anywhere (v6) (out)
description = PS3
command = ufw allow out 7777
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7777
iface = 
logging = 

[Rule24]
ufw_rule = 7496 (v6) ALLOW IN Anywhere (v6)
description = testForTWS
command = ufw allow in 7496
policy = allow
direction = in
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7496
iface = 
logging = 

[Rule25]
ufw_rule = 7496 (v6) ALLOW OUT Anywhere (v6) (out)
description = testForTWS
command = ufw allow out 7496
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 7496
iface = 
logging = 

